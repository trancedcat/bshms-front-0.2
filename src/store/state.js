import menu from '../data/menu'
import tab from '../data/tab'
import fft from '../data/fft'

export default{
    menu,
    tab,
    fft
}