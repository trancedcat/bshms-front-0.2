import Vue from 'vue'
import Router from 'vue-router'
import Monitoring from './views/Monitoring.vue'
import DataManagement from './views/DataManagement.vue'
import Admin from './views/Admin.vue'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/home',
      name: 'home',
      component: Home
    },
    {
      path: '/monitoring/:itemname',
      name: 'monitoring',
      component: Monitoring
    },
    {
      path: '/datamanagement/:itemname',
      name: 'datamanagement',
      component: DataManagement

    },
    {
      path: '/admin/:itemname',
      name: 'admin',
      component: Admin
    }

    
  ]
})
