export default [
    {
        name: '主页',
        alias: 'main',
        id: 1,
        parentid: 0,
        accesslevel: 1,
        icon: 'el-icon-s-home',
        path: '/',
        children: null
    },
    {
        name: '实时监测',
        alias: 'monitoring',
        id: 2,
        parentid: 0,
        accesslevel: 1,
        icon: 'el-icon-s-marketing',
        path: '/monitoring',
        children: [
            {
                name: '风速',
                alias: 'wind',
                id: 3,
                parentid: 2,
                accesslevel: 1,
                icon: null,
                path: '/monitoring/wind',
                children: null
            },
            {
                name: '路面温度',
                alias: 'roadtemp',
                id: 4,
                parentid: 2,
                accesslevel: 1,
                icon: null,
                path: '/monitoring/roadtemp',
                children: null
            },
            {
                name: '湿度',
                alias: 'humidity',
                id: 5,
                parentid: 2,
                accesslevel: 1,
                icon: null,
                path: '/monitoring/humidity',
                children: null
            },
            {
                name: '交通荷载',
                alias: 'traffic',
                id: 6,
                parentid: 2,
                accesslevel: 1,
                icon: null,
                path: '/monitoring/traffic',
                children: null
            },
            {
                name: '动力响应',
                alias: 'dynamic',
                id: 7,
                parentid: 2,
                accesslevel: 1,
                icon: null,
                path: '/monitoring/dynamic',
                children: null
            },
            {
                name: '应力应变',
                alias: 'stress',
                id: 8,
                parentid: 2,
                accesslevel: 1,
                icon: null,
                path: '/monitoring/stress',
                children: null
            },
            {
                name: '位移',
                alias: 'displacement',
                id: 9,
                parentid: 2,
                accesslevel: 1,
                icon: null,
                path: '/monitoring/displacement',
                children: null
            },
            {
                name: '索力',
                alias: 'cable',
                id: 10,
                parentid: 2,
                accesslevel: 1,
                icon: null,
                path: '/monitoring/cable',
                children: null
            },
            {
                name: 'GPS',
                alias: 'gps',
                id: 11,
                parentid: 2,
                accesslevel: 1,
                icon: null,
                path: '/monitoring/gps',
                children: null
            }

        ]
    },
    {
        name: '数据管理',
        alias: 'datamanagement',
        id: 12,
        parentid: 0,
        accesslevel: 1,
        icon: 'el-icon-s-data',
        path: '/datamanagement',
        children: [
            {
                name: '数据处理',
                alias: 'processing',
                id: 13,
                parentid: 12,
                accesslevel: 1,
                icon: null,
                path: '/datamanagement/processing',
                children: null
            },
            {
                name: '数据分析',
                alias: 'analysis',
                id: 14,
                parentid: 12,
                accesslevel: 1,
                icon: null,
                path: '/datamanagement/analysis',
                children: null
            }
        ]
    },
    {
        name: '综合管理',
        alias: 'admin',
        id: 15,
        parentid: 0,
        accesslevel: 1,
        icon: 'el-icon-s-tools',
        path: '/datamanagement',
        children: [
            {
                name: '用户管理',
                alias: 'users',
                id: 16,
                parentid: 15,
                accesslevel: 1,
                icon: null,
                path: '/admin/users',
                children: null
            },
            {
                name: '传感器管理',
                alias: 'sensors',
                id: 17,
                parentid: 15,
                accesslevel: 1,
                icon: null,
                path: '/admin/sensors',
                children: null
            },
            {
                name: '站点管理',
                alias: 'sites',
                id: 18,
                parentid: 15,
                accesslevel: 1,
                icon: null,
                path: '/admin/sites',
                children: null
            }
        ]
    },
    {
        name: '云管理',
        alias: 'cloud',
        id: 19,
        parentid: 0,
        accesslevel: 1,
        icon: 'el-icon-platform-eleme',
        path: '/cloud',
        children: null
    }
]
