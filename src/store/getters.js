

export default{
    getTabs: (state) => {
        return state.tab.name
    },

    getFFTAmount: (state) => {
        return state.fft.length
    }
}